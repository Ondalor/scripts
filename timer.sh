#!/bin/bash

now=$(date +"%T")
nowSecs=$(date +%s)
dst=$(date -v +$1M).

function trap_ctrlc ()
{
    new_now=$(date +"%T")
    newNowSecs=$(date +%s)
    duration=$(((newNowSecs-nowSecs)/60))

    echo "\nStopping the timer at $new_now, duration = $duration minutes"
 
    exit 2
}

trap "trap_ctrlc" 2

setalarm() {
    echo "Current time : $now"
    echo "Destination time: $dst"
    sleep $(echo "$1 * 60" | bc)
    osascript -e 'display notification "'"$1 minute timer complete"'" with title "Ding!"'
}

setalarm $1
