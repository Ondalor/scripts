#!/bin/bash

# open /Applications/Fork.app
open /System/Applications/Mail.app
open /System/Applications/Calendar.app
# open /Applications/Xcode.app
open /Applications/Xcode.app/Contents/Developer/Applications/Simulator.app

# open /Applications/Postman.app
# open /Applications/Proxyman.app
open /Applications/AppCode.app
