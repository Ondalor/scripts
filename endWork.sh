#!/bin/bash

pkill -x Fork
pkill -x Postman
pkill -x Proxyman
pkill -x Mail
pkill -x Calendar
pkill -x Xcode
pkill -x Simulator
pkill -x appcode
osascript -e 'quit app "Visual Studio Code"'
osascript -e 'quit app "WireGuard"'
osascript -e 'quit app "Reveal"'
osascript -e 'quit app "Paw"'